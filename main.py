import json
from flask import Flask, render_template,request,send_file,make_response,Response,url_for
from datetime import datetime
import requests
import datetime
import pytz
import re
from operator import itemgetter
import csv
# import socket
# import pandas as pd
from flask_caching import Cache
from flask_compress import Compress
# import asyncio
# import aiohttp

app = Flask(__name__)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})
@app.route('/')

# url for home page
def home():
    return render_template('Login.html')

# url for StudentDetails page
@app.route('/StudentDetails', methods=['GET', 'POST'])

# StudentDetails function which fetches input from html page and send to StudentDetailsnewApicall funtion as input 
# and the sent the output of StudentDetailsnewApicall to html page

def StudentDetails():
    if request.method == 'POST':

        # Getting data from html request
        dayset = request.form.getlist('input-data-Day[]')
        Start_Time_set = request.form.getlist('Start_Time')
        End_Time_set = request.form.getlist('End_Time')

        # initializing student_data variable 
        student_data = {}
    
       # condition- when day input is given/ dayset is not empty
        if dayset:
           
            for day in dayset:
                # condition- when start_time and end_time input is given/ Start_Time_set and End_Time_set are not empty
                if Start_Time_set and End_Time_set:
                    for i, start_time in enumerate(Start_Time_set):
                        end_time = End_Time_set[i]

                        #calling StudentDetailsnewApicall with day, start_time, end_time as input and storing return value in data1 variable
                        data1 = StudentDetailsnewApicall(day, start_time, end_time)

                        #storing start_time and end_time in time_slot
                        time_slot = (start_time, end_time)

                        #condition- if time_slot is in student_data then in student_data variable (list) data1 will be included one by one
                        if time_slot in student_data:
                            student_data[time_slot].extend(data1)

                        #condition- if time_slot is in  not student_data then in student_data variable (list) data1 will be included once and break
                        else:
                            student_data[time_slot] = data1

                # condition- when Start_Time_set and End_Time_set are empty then for StudentDetailsnewApicall the input will be day and other two arguments will be empty             
                else:
                    data1 = StudentDetailsnewApicall(day, "", "")
                    if data1:
                        student_data[day] = data1
        
       # condition- when day input is not given / dayset is empty 
       # condition- when Start_Time_set and End_Time_set is not empty then StudentDetailsnewApicall input will be only start_time and end_time    
        else:
            if Start_Time_set and End_Time_set:
                for i, start_time in enumerate(Start_Time_set):
                    end_time = End_Time_set[i]
                    # print('start_time ',start_time,'end_time ',end_time)
                    data1 = StudentDetailsnewApicall("", start_time, end_time)

                    #storing start_time and end_time in time_slot
                    time_slot = (start_time, end_time)

                    # print('time_slot',time_slot)

                    # print('student_data',student_data)

                    #condition- if time_slot is in student_data then in student_data variable (list) data1 will be included one by one
                    if time_slot in student_data:    
                        student_data[time_slot].extend(data1)


                        # print('student_data',student_data)
                        # print('data1',data1)

                   #condition- if time_slot is in  not student_data then in student_data variable (list) data1 will be included once and break      
                    else:
                        student_data[time_slot] = data1

            # condition- when Start_Time_set and End_Time_set are empty then for StudentDetailsnewApicall the input will be null
            else:
                data1 = StudentDetailsnewApicall("", "", "")

                # condition- when data1 is not empty then for All Day data1 will come 
                if data1:
                    student_data["All Day"] = data1
                else:
                    return render_template('StudentDetailsQuery.html', data="No Students Available")
     
        # print('student_data',student_data)
        # print('type of student_data',type(student_data))    
        
        # condition- if student_data is not empty then
        if student_data:
        
        # Open a CSV file named 'my_file.csv' for writing
         with open('my_file.csv', 'w', newline='') as f:

            # Create a CSV writer object with specified fieldnames
            fieldnames = ['Start Time', 'End Time', 'Student Name', 'Student Surname', 'Email', 'Time Slot', 'Day']
            writer = csv.DictWriter(f, fieldnames=fieldnames)
             
            # Write the header row to the CSV file
            writer.writeheader()
            
            # Iterate through each time_slot and associated students in student_data
            for time_slot, students in student_data.items():
                start_time, end_time = time_slot
                
                # Iterate through each student in the current time_slot
                for student in students:
                    # Write a row to the CSV file with student details
                    writer.writerow({
                        'Start Time': start_time,
                        'End Time': end_time,
                        'Student Name': student['student_name'],
                        'Student Surname': student['student_surname'],
                        'Email': student['student_email'],
                        'Time Slot': student['slot_time'],
                        'Day': student['slot_day']})
            
            # Set the download link for the CSV file
            download_link = '/download_csv'
            
           
        # Condition: If student_data is not empty        
        if student_data:

            # Render the template 'StudentDetailsQuery.html' with data, start_time, end_time, and download_link
            return render_template('StudentDetailsQuery.html', data=student_data, Start_Time=start_time, End_Time=end_time,download_link=download_link)
        
        else:

            # Render the template 'StudentDetailsQuery.html' with a message when no students are available
            return render_template('StudentDetailsQuery.html', data="No Students Available")

    # Default case: Render the template 'StudentDetailsQuery.html' without data
    return render_template('StudentDetailsQuery.html')
        

# function oathInfo for API authintication
def oathInfo(client_id,client_secret,auth_url,grant_type):
    response = requests.post(auth_url, data={"grant_type": grant_type, "client_id": client_id, "client_secret": client_secret})
    if response.status_code == 200:
        access_token = response.json()["access_token"]
  
    return access_token



# function ApiCall with the input number of pages and itemsperpage
def ApiCall(number1,number2):
  url="******"
  url2="****"
  new_url=url+str(number1)+url2+str(number2)
#   print('new_url',new_url)    
  return new_url

# function perpagedata with the input data and itemsperpage initial number
# This function works for autochange numberofitems per page in Apicall according to length of data
def perpagedata(totaldata,mindataperpage):
 
# variable lengthofData set to 500  
 lengthofData=500

#  condition- if total length of data is greter than lengthofData
 if(len(totaldata)<lengthofData):

    perpagevalue = max(mindataperpage, len(totaldata))
 else:
    perpagevalue = min(mindataperpage, len(totaldata))
 return perpagevalue
       


# function StudentDetailsnewApicall with the input day, start_time and end_time       
def StudentDetailsnewApicall(day, start_time, end_time):
    # print('entering into access_token')

    # Calling function oathInfo with the arguments client_id,client_secret,auth_url,grant_type  
    access_token=oathInfo("**","**","**","**")

    # print('access_token',access_token) 
    # print('entering into loop')

     # initialization of result
    result = []

    # initialization of page_number
    page_number=1

    # initialization of min_number_of_elements 
    min_number_of_elements = 500

    number_of_elements = min_number_of_elements
  
    while True:
        # print('page_number', page_number)

        # calling function ApiCall with arguments page_number and number_of_elements
        items_url = ApiCall(page_number,number_of_elements)
        headers = {"Authorization": f"Bearer {access_token}"}
        response = requests.get(items_url, headers=headers)

        if response.status_code == 200:
            # calling function StudentDetailsnew with day, start_time, end_time, response and storing result in result1 variable
            result1 = StudentDetailsnew(day, start_time, end_time, response)
            if not result1:
                break  
            # data will be added in result variable (list)
            result.extend(result1)

            # function perpagedata call with arguments result1  and min_number_of_elements
            number_of_elements=perpagedata(result1,min_number_of_elements)

            #page_number increase by one
            page_number += 1   

            # print('min_number_of_elements',min_number_of_elements)
            # print(' len(result)', len(result1))
            
        
        else:
            print(f"Failed to retrieve data for page {page_number}. Status code: {response.status_code}")
            break

    # print('result',result)
    return result



# function StudentDetailsnew with the day, start_time and end_time,response 
def StudentDetailsnew(day, start_time, end_time,response):
        
        if response.status_code == 200:

            # response data will be storing in output_data variable in json format
            output_data = response.json()

            # Initializing valiable student_details as list
            student_details = []

            # Initializing valiable start_time_part_1 and end_time_part_1
            start_time_part_1 = ""
            end_time_part_1= ""

            # it will iterate all the data of output_data and item['data']
              
            for item in output_data:
                for data_item in item['data']:

                    # Check if the 'fieldId' of the current data_item is 4654460
                    if data_item['fieldId'] == 4654460:

                        # Extracting the 'data' field value which is next to 'fieldId': 4654460,  and split it using '||' as the separator
                        field_value_Preferred_time = data_item['data']
                        split_data = field_value_Preferred_time.split('||')

                        # Iterating through each slot in the split_data
                        for slot in split_data:

                            # Spliting each slot using '|' as the separator
                            slot_parts = slot.split('|')

                            # Checking if there are at least two parts in the slot
                            if len(slot_parts) >= 2: 

                                # Extracting slot_time and slot_day from slot_parts
                                if slot_parts[0] == '':
                                    slot_time = slot_parts[1]
                                    # print('slot_time',slot_time)
                                    
                                    slot_day = slot_parts[2]
                                    # print('slot_day',slot_day)
                                else:
                                    slot_time = slot_parts[0]
                                    # print('slot_time',slot_time)
                                    
                                    slot_day = slot_parts[1]
                                    # print('slot_day',slot_day)

                                # Extracting hours and minutes from start_time and end_time
                                # Converting slot_time to 24-hour format
                                time_parts = slot_time.split('-')

                                # print('time_parts',time_parts)
                                time_parts_parts1=time_parts[0]

                                # time_obj_1 = parser.parse(time_parts_parts1)
                                time_obj_1=datetime.datetime.strptime(time_parts_parts1, "%I:%M %p")
                                time_parts1 = time_obj_1.strftime("%H:%M")
                                # print('time_parts1',time_parts1)

                                time_parts_parts2=time_parts[1]
                                # time_obj_2 = parser.parse(time_parts_parts2)

                                time_obj_2=datetime.datetime.strptime(time_parts_parts2, "%I:%M %p")
                                time_parts2 = time_obj_2.strftime("%H:%M")
                                # print('time_parts2',time_parts2)
                                # print('time_parts1',time_parts1,' time_parts2',time_parts2)

                                time_parts2_part=time_parts2.split(':')
                                # print('time_parts2_part',time_parts2_part)
                                time_parts1_part=time_parts1.split(':')

                                time_parts1_part1=time_parts1_part[0]
                                time_parts1_part2=time_parts1_part[1]
                                # print('time_parts1_part1 ',time_parts1_part1,'time_parts1_part2 ',time_parts1_part2)

                                time_parts2_part1=time_parts2_part[0]
                                time_parts2_part2=time_parts2_part[1]
                                # print('time_parts2_part1 ',time_parts2_part1,'time_parts2_part2 ',time_parts2_part2)
                         
                                # Extracting hours and minutes from end_time
                                if end_time:
                                        end_time_part=end_time.split(':')
                                        end_time_part_1=end_time_part[0]
                                        end_time_part_2=end_time_part[1]
                                        # print('end_time_part_1 ',end_time_part_1,'end_time_part_2 ',end_time_part_2)

                                # Extracting hours and minutes from start_time        
                                if start_time:
                                        start_time_part=start_time.split(':')
                                        start_time_part_1= start_time_part[0]
                                        start_time_part_2= start_time_part[1]
                                        # print('start_time_part_1 ',start_time_part_1,'start_time_part_2 ',start_time_part_2)
                                # print("day,start_time,end_time",day,start_time,end_time)
                                
                                # Check conditions for filtering slots
                                if (not day or day in slot_day) and ((not start_time and not end_time) or (not start_time or (start_time <= time_parts1 and time_parts2 <= end_time)) or (not end_time or (start_time <= time_parts1 and time_parts2 <= end_time))):

                                    # Iterate through each data_item in the current item's 'data'
                                    for data_item in item['data']:

                                        # Extract student details based on 'fieldId'
                                        # the data next to 'fieldId': 4536834 is 
                                        if data_item['fieldId'] == 4536834:
                                            student_name = data_item['data']
                                            # print('student_name',student_name)

                                        elif data_item['fieldId'] == 4536868:
                                            student_surname = data_item['data']
                                            # print('student_surname',student_surname)

                                        elif data_item['fieldId'] == 4536831:
                                            student_email = data_item['data']
                                            # print('student_email',student_email)
                                        # for time in slot_time:
                                   
                                    # Append student details to student_details list
                                    # print('student_name',student_name)
                                    if student_name:
                                                if day and not start_time and not end_time:
                                                    student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                #    print ('student_details 0',student_details)
                                                    
                                                elif (start_time and not end_time) or (day and start_time and not end_time):
                                                    # Check conditions for adding slots to student_details
                                                    # print('slot_time ',slot_time,' end_time ',end_time)
                                                    # print('comparison between time_parts1_part1 ',time_parts1_part1 ,' and start_time_part_1 ',start_time_part_1)

                                                    if time_parts1_part1==start_time_part_1  and time_parts2 <= "21:00":
                                                        # print('comparison between time_parts1_part2 ',time_parts1_part2,' and start_time_part_2  ',start_time_part_2)

                                                        if time_parts1_part2>=start_time_part_2:
    
                                                         student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                        #  print('student_details 1',student_details)
                                                   
                                                    elif time_parts1_part1>start_time_part_1 and time_parts2 <= "21:00":
                                            
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 1',student_details)
                                                    else:
                                                        print("no data")
                                             
                                                elif (end_time and not start_time) or (day and end_time and not start_time):
                                                     # Check conditions for adding slots to student_details
                                                    # print('slot_time ',slot_time,' end_time ',end_time)
                                                    # print('comparison between time_parts2_part1 ',time_parts2_part1," and end_time_part_1 ",end_time_part_1)

                                                    if time_parts2_part1==end_time_part_1 and time_parts1>="01:00":
                                                        # print('comparison between time_parts2_part2 ',time_parts2_part2," and end_time_part_2 ",end_time_part_2)

                                                        if time_parts2_part2<=end_time_part_2:
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 2 ',student_details)
                                                        else:
                                                            print('No data in output')
                                                            # print('comparison between time_parts2_part1 ',time_parts2_part1," and end_time_part_1 ",end_time_part_1)

                                                    elif time_parts2_part1<end_time_part_1 and time_parts1>="01:00":
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 2 ',student_details)
                                                    else:
                                                        print("no data")
                                                    # print('starttime before loop',start_time)
                                                        
                                                elif (start_time and end_time) or (day and start_time and end_time):
                                                     # Check conditions for adding slots to student_details
                                                    # print('slot_time ',slot_time)
                                                    # print('comparison between time_parts1_part1 ',time_parts1_part1," and start_time_part_1 ",start_time_part_1,'and time_parts2_part1 ',time_parts2_part1," and end_time_part_1 ",end_time_part_1)                    
                                                                                    
                                                    if time_parts1_part1==start_time_part_1 and time_parts2_part1==end_time_part_1:
                                                        # print('comparison between time_parts1_part2 ',time_parts1_part2," and start_time_part_2 ",start_time_part_2,'and time_parts2_part2',time_parts2_part2," and end_time_part_2",end_time_part_2)  

                                                        if time_parts1_part2>=start_time_part_2 and time_parts2_part2<=end_time_part_2 :  
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 3_1',student_details)

                                                    elif time_parts1_part1>start_time_part_1 and time_parts2_part1==end_time_part_1:
                                                        # print('comparison between time_parts2_part2 ',time_parts2_part2," and start_time_part_2 ",start_time_part_2,'and time_parts2_part2',time_parts2_part2," and end_time_part_2",end_time_part_2)  

                                                        if  time_parts2_part2<=end_time_part_2 :  
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 3_2',student_details)

                                                    elif time_parts1_part1==start_time_part_1 and time_parts2_part1<end_time_part_1:
                                                        # print('comparison between time_parts1_part2 ',time_parts1_part2," and start_time_part_2 ",start_time_part_2,'and time_parts2_part2',time_parts2_part2," and end_time_part_2",end_time_part_2)  

                                                        if  time_parts1_part2>=start_time_part_2:  
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                    elif time_parts1_part1>start_time_part_1 and time_parts2_part1<end_time_part_1:
                                                            student_details.append({'student_name': student_name, 'student_surname': student_surname,'student_email': student_email,'slot_time': slot_time, 'slot_day': slot_day,'start_time': start_time,'end_time': end_time})
                                                            # print('student_details 3_4',student_details)
                                                    else:
                                                        print("No data")
                                                else:
                                                    print('No data found')

            # Printing the final student_details list and return it
            # print('student_details',student_details)
            return student_details
    
        # If no matching slots are found, return an empty list
        return []


# the code is for downloading the current data as a csv file
@app.route('/download_csv')


# function download_csv for downloading the current data as a csv file
def download_csv():
    # Opening the CSV file
    with open('my_file.csv', 'r') as f:

        # Creating a response with the file content
        response = make_response(f.read())

        # Setting the appropriate headers for file download
        response.headers['Content-Disposition'] = 'attachment; filename=my_file.csv'
        response.headers['Content-Type'] = 'text/csv'

    return response                                         



if __name__ == "__main__":
    app.run(debug=True)