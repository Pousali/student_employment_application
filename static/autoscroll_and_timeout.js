 // time limit for parallex section
 window.setTimeout(function() {
    scrollToSection('section2');
}, 500); 

// Autoscrolling for parallex sextion
function scrollToSection(sectionId) {
    document.getElementById(sectionId).scrollIntoView({
        behavior: 'smooth'
    });
}